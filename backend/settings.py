import os

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_DIR = os.path.join(PROJECT_DIR, 'frontend/dist')
checkpoint_path = os.path.join(PROJECT_DIR, "backend/data/model/mnist_cnn.pt")
