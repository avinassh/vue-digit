import json
import os

from tornado import web, ioloop, log
from tornado.options import parse_command_line
from tornado.options import define, options
from sockjs.tornado import SockJSRouter, SockJSConnection

from predict import predict
from settings import STATIC_DIR

logger = log.logging.getLogger("tornado.general")

define("port", default="9999", help="HTTP listening port", type=str)


class IndexHandler(web.RequestHandler):

    def get(self):
        self.render(os.path.join(STATIC_DIR, 'index.html'))


class EchoConnection(SockJSConnection):
    def on_message(self, msg):
        try:
            request = json.loads(msg)
            image_data = request['image_data']
        except(ValueError, KeyError):
            return
        prediction = predict(image_data=image_data)
        accuracy, val = prediction.max(1)
        # we get accuracy value like `0.9274999499320984`
        # so we multiply by 100 to get `92.74999499320984`
        # then we set the float to two decimal points to get `92.74`
        accuracy = float("{0:.2f}".format(accuracy.item() * 100))
        response = {'accuracy': accuracy, 'value': val.item()}
        logger.info('sending: {}'.format(response))
        self.send(response)

    def on_open(self, info):
        logger.info('new connection opened')
        self.send('hello!')

    def on_close(self):
        logger.info('closing the connection')


if __name__ == '__main__':
    parse_command_line()
    port = options.port
    logger.info(F'starting the server at {port}')
    handlers = [(r'/', IndexHandler),
                (r'/(.*)', web.StaticFileHandler, {'path': STATIC_DIR})]
    EchoRouter = SockJSRouter(EchoConnection, '/socks')
    EchoRouter.urls.extend(handlers)
    app = web.Application(EchoRouter.urls, debug=True)
    app.listen(port)
    ioloop.IOLoop.instance().start()
