import torch

from train import NeuralNet
from settings import checkpoint_path


def get_model():
    model = NeuralNet()
    model.load_state_dict(torch.load(checkpoint_path))
    model.eval()
    return model
