# Vue Digit

## How it works?

Everything is drawn on a HTML5 canvas of 280px x 280px, then it is resized to 28px x 28px size and converted to greyscale. Then all the pixel values, 784 are sent to the backend server where it is run through a simple MNP:


    class NeuralNet(nn.Module):
        def __init__(self):
            super(NeuralNet, self).__init__()
            self.fc1 = nn.Linear(784, 512)
            self.fc2 = nn.Linear(512, 256)
            self.fc3 = nn.Linear(256, 128)
            self.fc4 = nn.Linear(128, 64)
            self.fc5 = nn.Linear(64, 10)
            self.dropout = nn.Dropout(0.3)

The communication layer between frontend and backend is done using SockJS.

## Requirements

Install them from `requirements.txt`:

    pip install -r requirements.txt

## Deployment

Train the model:

    python train.py


Then run the server:

    python main.py