import torch


from utils import get_model

model = get_model()
model.eval()


def predict(image_data):
    global model
    imageTensor = torch.FloatTensor([image_data])
    imageTensor = (imageTensor - 0.5) / 0.5
    with torch.no_grad():
        logps = model(imageTensor)
    ps = torch.exp(logps)
    return ps
