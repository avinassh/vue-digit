module.exports = {
  configureWebpack: {
    module: {
      rules: [
        {
          test: /.html$/,
          loader: "vue-template-loader",
          exclude: /index.html/
        }
      ]
    }
  },
devServer: {
  host: '0.0.0.0',
  hot: true,
  disableHostCheck: true,
  },
}