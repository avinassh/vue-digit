import { DirectiveOptions, VNode, VNodeDirective } from 'vue';

interface MousePosition {
    offsetX: number;
    offsetY: number;
}

export interface CustomImageData {
    data: number[];
}

const drawingBoard: DirectiveOptions = {
    inserted(el: HTMLElement, binding: VNodeDirective, vnode: VNode) {
        const canvas = el as HTMLCanvasElement;
        const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        canvas.width = 280;
        canvas.height = 280;
        ctx.lineJoin = 'round';
        ctx.lineCap = 'round';
        ctx.lineWidth = 10;
        ctx.strokeStyle = 'white';

        let prevPosition = { offsetX: 0, offsetY: 0 } as MousePosition;
        let isDrawing = false;

        // TODO: clear this mess!
        vnode.context.$vnode.componentInstance.$on('clearButtonClicked', () => {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
        });

        function handleMouseDown(e: MouseEvent) {
            isDrawing = true;
            prevPosition = e;
        }

        function imageDataToGrayscale(imgData: ImageData) {
            const grayscaleImg = new Array<number>();
            for (let i = 0; i < imgData.data.length; i += 4) {
                const greyScale = 0.2989 * imgData.data[i] +
                    0.5870 * imgData.data[i + 1] + 0.1140 * imgData.data[i + 2];
                grayscaleImg.push(parseFloat((greyScale / 255).toFixed(4)));
            }
            return grayscaleImg;
        }

        function endPaintEvent() {
            isDrawing = false;
            const resizedCanvas = document.getElementById('resized-canvas') as HTMLCanvasElement;
            const resizedContext = resizedCanvas.getContext('2d') as CanvasRenderingContext2D;
            resizedCanvas.height = 28;
            resizedCanvas.width = 28;
            resizedContext.drawImage(canvas, 0, 0, 28, 28);
            const myImageData = resizedContext.getImageData(0, 0, resizedCanvas.width, resizedCanvas.height);
            const greyScaledImage = imageDataToGrayscale(myImageData);
            const eventData: CustomImageData = { data: greyScaledImage };
            if (binding.expression && vnode && vnode.context && vnode.context[binding.expression]) {
                vnode.context[binding.expression](eventData);
            }
        }

        function handleMouseMove(e: MouseEvent) {
            if (isDrawing) {
                draw(prevPosition, e);
            }
        }

        function draw(start: MousePosition, stop: MousePosition) {
            ctx.beginPath();
            ctx.moveTo(start.offsetX, start.offsetY);
            ctx.lineTo(stop.offsetX, stop.offsetY);
            ctx.stroke();
            prevPosition = stop;
        }

        canvas.addEventListener('mousedown', handleMouseDown);
        canvas.addEventListener('mousemove', handleMouseMove);
        canvas.addEventListener('mouseup', endPaintEvent);
        canvas.addEventListener('mouseleave', endPaintEvent);
    },
};

export default drawingBoard;
