import SockJS from 'sockjs-client';

class Connection {
    public closeEvent = 'close';
    public onMessageEvent = 'message';
    public onErrorEvent = 'error';

    private sock: WebSocket;
    private url: string = '/socks';

    constructor() {
        this.sock = new SockJS(this.url) as WebSocket;
        this.sock.onopen = () => {
            this.updateEventListeners();
        };
    }

    get isConnected(): boolean {
        if (!this.sock || this.sock.url === '') {
            return false;
        }
        if (this.sock.readyState === SockJS.CONNECTING || this.sock.readyState === SockJS.OPEN) {
            return true;
        }
        return false;
    }

    public connect(force = false) {
        if (force) {
            if (this.sock) {
                this.sock.close(0, 'closing');
            }
            this.sock = new SockJS(this.url) as WebSocket;
            return;
        }
        if (this.isConnected) {
            return;
        }
        this.sock = new SockJS(this.url) as WebSocket;
    }

    public onClose(this: Connection) {
        let power = 1;
        const timeOutFunc = function(this: Connection) {
            const delay = (Math.pow(2, power) - 1) * 500;
            power += 1;
            if (power > 7) {
                power = 7;
            }
            this.connect();
            if (!this.isConnected) {
                setTimeout(timeOutFunc.bind(this), delay);
            }
        };
        setTimeout(timeOutFunc.bind(this), 0);
    }

    public send(msg: string) {
        this.sock.send(msg);
    }

    public registerEventListener(eventType: string, listener: EventListenerOrEventListenerObject) {
        this.sock.addEventListener(eventType, listener);
    }

    private updateEventListeners() {
        this.sock.addEventListener(this.closeEvent, this.onClose.bind(this));
        this.sock.addEventListener(this.onErrorEvent, this.onClose.bind(this));
    }
}

const connection = new Connection();

export default connection;
