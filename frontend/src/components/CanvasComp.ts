import { Component, Vue } from 'vue-property-decorator';

import drawingBoard, { CustomImageData } from '../directives/drawingboard';
import connection from '../lib/Connection';
import WithRender from './canvas.html';

interface PredictionResponse {
  accuracy: number;
  value: number;
}

@WithRender
@Component({
  directives: {
    drawingBoard,
  },
})
export default class CanvasComp extends Vue {
  private myTimerId: number = 0;
  private drawingData: number[] = [];
  private accuracy: number = -1;
  private prediction: number = -1;

  public mounted() {
    connection.connect();
    connection.registerEventListener(connection.onMessageEvent, this.onMessageEventHandler.bind(this));
  }

  public data() {
    return { accuracy: this.accuracy, prediction: this.prediction };
  }

  public onButtonClick() {
    this.drawingData = [];
    this.accuracy = this.prediction = -1;
    this.$emit('clearButtonClicked');
  }

  public onMessageEventHandler(ev) {
    const predictionResponse = ev.data as PredictionResponse;
    this.accuracy = predictionResponse.accuracy;
    this.prediction = predictionResponse.value;
  }

  public sendData() {
    connection.send(JSON.stringify({ image_data: this.drawingData }));
  }

  public handleEndPaint(payload: CustomImageData) {
    this.accuracy = this.prediction = -1;
    this.drawingData = payload.data;
    if (this.myTimerId) {
      clearTimeout(this.myTimerId);
    }
    this.myTimerId = setTimeout(() => {
      this.sendData();
    }, 1000);
  }
}
